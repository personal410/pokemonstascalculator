//
//  SelectPokemonTableViewController.swift
//  PokemonStatsCalculator
//
//  Created by victor salazar on 30/05/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
class SelectPokemonTableViewController:UITableViewController{
    let arrPokemons = Pokemon.getAllPokemons()
    // MARK: - Table view data source
    override func tableView(_ tableView:UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPokemons.count
    }
    override func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = arrPokemons[indexPath.row].name
        return cell
    }
    override func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath){
        let viewCont = self.navigationController?.viewControllers[0] as! PokemonStatCalculatorViewController
        viewCont.pokemon = arrPokemons[indexPath.row]
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
}
