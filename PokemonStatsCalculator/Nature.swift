//
//  Nature.swift
//  PokemonStatsCalculator
//
//  Created by Victor Salazar on 31/05/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import Foundation
class Nature{
    var name:String
    var increaseStat:Int
    var decreaseStat:Int
    init(nam:String, increase:Int, decrease:Int){
        self.name = nam
        self.increaseStat = increase
        self.decreaseStat = decrease
    }
}