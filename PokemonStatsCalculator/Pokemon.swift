//
//  Pokemon.swift
//  PokemonStatsCalculator
//
//  Created by Victor Salazar on 25/05/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import Foundation
import CoreData
class Pokemon:NSManagedObject{
    class func addPokemon(_ name:String, healthPoints:Int, attack:Int, defense:Int, specialAttack:Int, specialDefense:Int, speed:Int){
        let ctxt = AppDelegate.getMoc()
        let newPokemon = NSEntityDescription.insertNewObject(forEntityName: "Pokemon", into: ctxt) as! Pokemon
        newPokemon.name = name
        newPokemon.health_points = NSNumber(integerLiteral: healthPoints)
        newPokemon.attack = NSNumber(integerLiteral: attack)
        newPokemon.defense = NSNumber(integerLiteral: defense)
        newPokemon.special_attack = NSNumber(integerLiteral: specialAttack)
        newPokemon.special_defense = NSNumber(integerLiteral: specialDefense)
        newPokemon.speed = NSNumber(integerLiteral: speed)
        try! ctxt.save()
    }
    class func getAllPokemons() -> [Pokemon]{
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Pokemon")
        fetchReq.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        do{
            return try ctxt.fetch(fetchReq) as! [Pokemon]
        }catch let error as NSError {
            print("error: \(error)")
            return []
        }
    }
}
