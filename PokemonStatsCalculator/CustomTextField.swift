//
//  CustomTextField.swift
//  PokemonStatsCalculator
//
//  Created by victor salazar on 30/05/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
class CustomTextField:UITextField{
    @IBInspectable var hasBorder:Bool = false
    var intValue:Int {
        get{
            let text = self.text! as NSString
            return text.integerValue
        }
    }
    override func draw(_ rect:CGRect){
        self.layer.borderWidth = hasBorder ? 1 : 0
    }
}
