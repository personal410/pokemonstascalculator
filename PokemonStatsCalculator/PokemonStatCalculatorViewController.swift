//
//  PokemonStatCalculatorViewController.swift
//  PokemonStatsCalculator
//
//  Created by Victor Salazar on 25/05/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
class PokemonStatCalculatorViewController:UIViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    //MARK: - Variables
    var pokemon:Pokemon?
    var currentNatureIndex = 0 {
        didSet{
            let currentNature = arrNatures[currentNatureIndex]
            self.pokemonNatureTxtFld.text = currentNature.name
        }
    }
    var currentTxtFld:UITextField?
    var arrIVTxtFlds:[CustomTextField] = []
    var arrEVTxtFlds:[CustomTextField] = []
    var arrTotalLbls:[UILabel] = []
    let arrNatures = Toolbox.getAllNatures()
    //MARK: - IBOutlet
    @IBOutlet weak var pokemonNameTxtFld:UITextField!
    @IBOutlet weak var pokemonLevelTxtFld:CustomTextField!
    @IBOutlet weak var pokemonNatureTxtFld:UITextField!
    @IBOutlet weak var healthPointsBaseLbl:UILabel!
    @IBOutlet weak var attackBaseLbl:UILabel!
    @IBOutlet weak var defenseBaseLbl:UILabel!
    @IBOutlet weak var specialAttackBaseLbl:UILabel!
    @IBOutlet weak var specialDefenseBaseLbl:UILabel!
    @IBOutlet weak var speedBaseLbl:UILabel!
    @IBOutlet weak var healthPointsIVTxtFld:CustomTextField!
    @IBOutlet weak var attackIVTxtFld:CustomTextField!
    @IBOutlet weak var defenseIVTxtFld:CustomTextField!
    @IBOutlet weak var specialAttackIVTxtFld:CustomTextField!
    @IBOutlet weak var specialDefenseIVTxtFld:CustomTextField!
    @IBOutlet weak var speedIVTxtFld:CustomTextField!
    @IBOutlet weak var healthPointsEVTxtFld:CustomTextField!
    @IBOutlet weak var attackEVTxtFld:CustomTextField!
    @IBOutlet weak var defenseEVTxtFld:CustomTextField!
    @IBOutlet weak var specialAttackEVTxtFld:CustomTextField!
    @IBOutlet weak var specialDefenseEVTxtFld:CustomTextField!
    @IBOutlet weak var speedEVTxtFld:CustomTextField!
    @IBOutlet weak var healthPointsTotalLbl:UILabel!
    @IBOutlet weak var attackTotalLbl:UILabel!
    @IBOutlet weak var defenseTotalLbl:UILabel!
    @IBOutlet weak var specialAttackTotalLbl:UILabel!
    @IBOutlet weak var specialDefenseTotalLbl:UILabel!
    @IBOutlet weak var speedTotalLbl:UILabel!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        arrEVTxtFlds = [self.healthPointsEVTxtFld, self.attackEVTxtFld, self.defenseEVTxtFld, self.specialAttackEVTxtFld, self.specialDefenseEVTxtFld, self.speedEVTxtFld]
        arrIVTxtFlds = [self.healthPointsIVTxtFld, self.attackIVTxtFld, self.defenseIVTxtFld, self.specialAttackIVTxtFld, self.specialDefenseIVTxtFld, self.speedIVTxtFld]
        arrTotalLbls = [self.healthPointsTotalLbl, self.attackTotalLbl, self.defenseTotalLbl, self.specialAttackTotalLbl, self.specialDefenseTotalLbl, self.speedTotalLbl]
        currentNatureIndex = 0
        let currentNature = arrNatures[currentNatureIndex]
        self.arrTotalLbls[currentNature.decreaseStat].textColor = UIColor.red
        self.arrTotalLbls[currentNature.increaseStat].textColor = UIColor.blue
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        self.pokemonNatureTxtFld.inputView = pickerView
    }
    override func viewWillAppear(_ animated:Bool){
        super.viewWillAppear(animated)
        if pokemon != nil {
            self.pokemonNameTxtFld.text = pokemon?.name
            self.healthPointsBaseLbl.text = "\(pokemon!.health_points.intValue)"
            self.attackBaseLbl.text = "\(pokemon!.attack.intValue)"
            self.defenseBaseLbl.text = "\(pokemon!.defense.intValue)"
            self.specialAttackBaseLbl.text = "\(pokemon!.special_attack.intValue)"
            self.specialDefenseBaseLbl.text = "\(pokemon!.special_defense.intValue)"
            self.speedBaseLbl.text = "\(pokemon!.speed.intValue)"
            let level = (self.pokemonLevelTxtFld.text! as NSString).integerValue
            calculateAllParams(level)
        }
    }
    //MARK: - IBAction
    @IBAction func dismissKeyboard(){
        currentTxtFld?.resignFirstResponder()
    }
    //MARK: - UITextField
    func textFieldShouldBeginEditing(_ textField:UITextField) -> Bool {
        if textField == pokemonNameTxtFld {
            self.performSegue(withIdentifier: "selectPokemon", sender: nil)
            return false
        }
        currentTxtFld = textField
        return true
    }
    func textField(_ textField:UITextField, shouldChangeCharactersIn range:NSRange, replacementString string:String) -> Bool {
        let txtFld = textField as! CustomTextField
        let text = textField.text! as NSString
        let finalText = text.replacingCharacters(in: range, with: string) as NSString
        let value = finalText.integerValue
        if let index = arrEVTxtFlds.index(of: txtFld) {
            let level = pokemonLevelTxtFld.intValue
            let iv = self.arrIVTxtFlds[index].intValue
            calculateParams(index, iv: iv, ev: value, level: level)
        }else{
            if let index = arrIVTxtFlds.index(of: txtFld) {
                let level = pokemonLevelTxtFld.intValue
                let ev = self.arrEVTxtFlds[index].intValue
                calculateParams(index, iv: value, ev: ev, level: level)
            }else{
                calculateAllParams(value)
            }
        }
        return true
    }
    func textFieldDidEndEditing(_ textField:UITextField){
        if textField != pokemonNatureTxtFld {
            let txtFld = textField as! CustomTextField
            if arrEVTxtFlds.index(of: txtFld) != nil {
                let value = txtFld.intValue
                if value > 252 {
                    txtFld.text = "252"
                }
            }else{
                if arrIVTxtFlds.index(of: txtFld) != nil {
                    let value = txtFld.intValue
                    if value > 31 {
                        txtFld.text = "31"
                    }
                }else{
                    if txtFld == self.pokemonLevelTxtFld {
                        if txtFld.intValue > 100 {
                            txtFld.text = "100"
                        }
                    }
                }
            }
        }
    }
    //MARK: - UIPickerView
    func numberOfComponents(in pickerView:UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView:UIPickerView, numberOfRowsInComponent component:Int) -> Int {
        return arrNatures.count
    }
    func pickerView(_ pickerView:UIPickerView, titleForRow row:Int, forComponent component:Int) -> String? {
        return arrNatures[row].name
    }
    func pickerView(_ pickerView:UIPickerView, didSelectRow row:Int, inComponent component:Int){
        let previousNature = arrNatures[currentNatureIndex]
        let level = pokemonLevelTxtFld.intValue
        currentNatureIndex = row
        if previousNature.decreaseStat > -1 {
            let iv = self.arrIVTxtFlds[previousNature.decreaseStat].intValue
            let ev = self.arrEVTxtFlds[previousNature.decreaseStat].intValue
            calculateParams(previousNature.decreaseStat, iv: iv, ev: ev, level: level)
            let iv2 = self.arrIVTxtFlds[previousNature.increaseStat].intValue
            let ev2 = self.arrEVTxtFlds[previousNature.increaseStat].intValue
            calculateParams(previousNature.increaseStat, iv: iv2, ev: ev2, level: level)
            self.arrTotalLbls[previousNature.decreaseStat].textColor = UIColor.black
            self.arrTotalLbls[previousNature.increaseStat].textColor = UIColor.black
        }
        let currentNature = arrNatures[currentNatureIndex]
        let currentDecreaseStat = currentNature.decreaseStat
        if currentDecreaseStat > -1 {
            let iv = self.arrIVTxtFlds[currentDecreaseStat].intValue
            let ev = self.arrEVTxtFlds[currentDecreaseStat].intValue
            calculateParams(currentDecreaseStat, iv: iv, ev: ev, level: level)
            let currentIncreaseStat = currentNature.increaseStat
            let iv2 = self.arrIVTxtFlds[currentIncreaseStat].intValue
            let ev2 = self.arrEVTxtFlds[currentIncreaseStat].intValue
            calculateParams(currentIncreaseStat, iv: iv2, ev: ev2, level: level)
            self.arrTotalLbls[currentNature.decreaseStat].textColor = UIColor.red
            self.arrTotalLbls[currentNature.increaseStat].textColor = UIColor.blue
        }
    }
    //MARK: - Auxiliar
    func calculateParams(_ index:Int, iv:Int, ev:Int, level:Int){
        var hpIV = iv
        if hpIV > 31 {
            hpIV = 31
        }
        var hpEV = ev
        if hpEV > 252 {
            hpEV = 252
        }
        let finalHpEV = Int(floor(Double(hpEV) / 4.0))
        var finalLevel = level
        if finalLevel > 100 {
            finalLevel = 100
        }
        let arrBaseStat = [pokemon!.health_points.intValue, pokemon!.attack.intValue, pokemon!.defense.intValue, pokemon!.special_attack.intValue, pokemon!.special_defense.intValue, pokemon!.speed.intValue]
        var finalStat = Int(floor(Double(arrBaseStat[index] * 2 + hpIV + finalHpEV) * Double(finalLevel) / 100.0))
        if index == 0 {
            finalStat += 10 + Int(finalLevel)
        }else{
            let currentNature = arrNatures[currentNatureIndex]
            let factor = currentNature.decreaseStat == index ? 0.9 : (currentNature.increaseStat == index ? 1.1 : 1)
            finalStat = Int(floor(Double(finalStat + 5) * factor)) 
        }
        self.arrTotalLbls[index].text = "\(finalStat)"
    }
    func calculateAllParams(_ level:Int){
        for i in 0 ... 5 {
            let iv = self.arrIVTxtFlds[i].intValue
            let ev = self.arrEVTxtFlds[i].intValue
            calculateParams(i, iv: iv, ev: ev, level: level)
        }
    }
}
