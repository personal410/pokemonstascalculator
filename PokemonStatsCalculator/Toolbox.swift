//
//  Toolbox.swift
//  PokemonStatsCalculator
//
//  Created by Victor Salazar on 31/05/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import Foundation
class Toolbox{
    class func getAllNatures() -> Array<Nature> {
        var arrNatures:Array<Nature> = []
        arrNatures.append(Nature(nam: "Activa", increase: 5, decrease: 2))
        arrNatures.append(Nature(nam: "Afable", increase: 3, decrease: 2))
        arrNatures.append(Nature(nam: "Agitada", increase: 2, decrease: 3))
        arrNatures.append(Nature(nam: "Alegre", increase: 5, decrease: 3))
        arrNatures.append(Nature(nam: "Alocada", increase: 3, decrease: 4))
        arrNatures.append(Nature(nam: "Amable", increase: 4, decrease: 2))
        arrNatures.append(Nature(nam: "Audaz", increase: 1, decrease: 5))
        arrNatures.append(Nature(nam: "Cauta", increase: 4, decrease: 3))
        arrNatures.append(Nature(nam: "Dócil", increase: -1, decrease: -1))
        arrNatures.append(Nature(nam: "Firme", increase: 1, decrease: 3))
        arrNatures.append(Nature(nam: "Floja", increase: 2, decrease: 4))
        arrNatures.append(Nature(nam: "Fuerte", increase: -1, decrease: -1))
        arrNatures.append(Nature(nam: "Grosera", increase: 4, decrease: 5))
        arrNatures.append(Nature(nam: "Huraña", increase: 1, decrease: 2))
        arrNatures.append(Nature(nam: "Ingenua", increase: 5, decrease: 4))
        arrNatures.append(Nature(nam: "Mansa", increase: 3, decrease: 5))
        arrNatures.append(Nature(nam: "Miedosa", increase: 5, decrease: 1))
        arrNatures.append(Nature(nam: "Modesta", increase: 3, decrease: 1))
        arrNatures.append(Nature(nam: "Osada", increase: 2, decrease: 1))
        arrNatures.append(Nature(nam: "Pícara", increase: 1, decrease: 4))
        arrNatures.append(Nature(nam: "Plácida", increase: 2, decrease: 5))
        arrNatures.append(Nature(nam: "Rara", increase: -1, decrease: -1))
        arrNatures.append(Nature(nam: "Serena", increase: 4, decrease: 1))
        arrNatures.append(Nature(nam: "Seria", increase: -1, decrease: -1))
        arrNatures.append(Nature(nam: "Tímida", increase: -1, decrease: -1))
        return arrNatures
    }
}