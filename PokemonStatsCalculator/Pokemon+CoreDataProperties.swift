//
//  Pokemon+CoreDataProperties.swift
//  PokemonStatsCalculator
//
//  Created by Victor Salazar on 25/05/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
import Foundation
import CoreData
extension Pokemon{
    @NSManaged var name:String
    @NSManaged var health_points:NSNumber
    @NSManaged var attack:NSNumber
    @NSManaged var defense:NSNumber
    @NSManaged var special_attack:NSNumber
    @NSManaged var special_defense:NSNumber
    @NSManaged var speed:NSNumber
}