//
//  NewPokemonViewController.swift
//  PokemonStatsCalculator
//
//  Created by Victor Salazar on 25/05/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
class NewPokemonViewController:UIViewController{
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var nameTxtFld:UITextField!
    @IBOutlet weak var hpTxtFld:UITextField!
    @IBOutlet weak var attackTxtFld:UITextField!
    @IBOutlet weak var defenseTxtFld:UITextField!
    @IBOutlet weak var specialAttackTxtFld:UITextField!
    @IBOutlet weak var specialDefenseTxtFld:UITextField!
    @IBOutlet weak var speedTxtFld:UITextField!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(NewPokemonViewController.showKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NewPokemonViewController.hideKeyboard), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    //MARK: - IBAction
    @IBAction func save(){
        let name = nameTxtFld.text!
        let hp:NSString = hpTxtFld.text! as NSString
        let attack:NSString = attackTxtFld.text! as NSString
        let defense:NSString = defenseTxtFld.text! as NSString
        let specialAttack:NSString = specialAttackTxtFld.text! as NSString
        let specialDefense:NSString = specialDefenseTxtFld.text! as NSString
        let speed:NSString = speedTxtFld.text! as NSString
        var allFieldsFilled = true
        if name.isEmpty {
            allFieldsFilled = false
        }else if hp.length == 0 {
            allFieldsFilled = false
        }else if attack.length == 0 {
            allFieldsFilled = false
        }else if defense.length == 0 {
            allFieldsFilled = false
        }else if specialAttack.length == 0 {
            allFieldsFilled = false
        }else if specialDefense.length == 0 {
            allFieldsFilled = false
        }else if speed.length == 0 {
            allFieldsFilled = false
        }
        if allFieldsFilled {
            Pokemon.addPokemon(name, healthPoints: hp.integerValue, attack: attack.integerValue, defense: defense.integerValue, specialAttack: specialAttack.integerValue, specialDefense: specialDefense.integerValue, speed: speed.integerValue)
            _ = self.navigationController?.popToRootViewController(animated: true)
        }else{
            let alertCont = UIAlertController(title: "Alerta", message: "Debe completar todos los campos", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertCont, animated: true, completion: nil)
        }
    }
    //MARK: - Keyboard
    func showKeyboard(_ notification:Notification){
        var userInfo = notification.userInfo!
        let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height, right: 0)
        self.scrollView.contentInset = contentInset
        self.scrollView.scrollIndicatorInsets = contentInset
    }
    func hideKeyboard(){
        let contentInset = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
        self.scrollView.scrollIndicatorInsets = contentInset
    }
}
